#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<windows.h>
#include<conio.h>
#include<graphics.h>
#pragma comment(lib,"Winmm.lib")
#define width 350
#define high 600
//全局变量
int bird_x, bird_y;//鸟位置
int bar_x1, bar_y1;//上柱子位置
int bar_x2, bar_y2;//下柱子位置 
int judge;//判断
void startup() // 数据初始化
{
	mciSendString("open D:\\background.mp3 alias bkmusic ", NULL, 0, NULL);
	mciSendString("play bkmusic repeat", NULL, 0, NULL);
	initgraph(width, high);
	bird_x = 100;
	bird_y = high / 2;
	bar_x1 = 200;
	bar_y1 = -500;
	bar_x2 = 200;
	bar_y2 = 500;
	judge = 1;
	BeginBatchDraw();
}
void show()  // 显示画面
{

	IMAGE img_bg, img_bd1, img_bd2, img_zhuzishang1, img_zhuzishang2, img_zhuzixia1, img_zhuzixia2;
	loadimage(&img_bg, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\background.jpg");
	loadimage(&img_bd1, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bird1.jpg");
	loadimage(&img_bd2, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bird2.jpg");
	loadimage(&img_zhuzishang1, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bar_up1.gif");
	loadimage(&img_zhuzishang2, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bar_up2.gif");
	loadimage(&img_zhuzixia1, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bar_down1.gif");
	loadimage(&img_zhuzixia2, "D:\\GITTT\\tijiao\\sucai\\flappy bird\\bar_down2.gif");
	putimage(0, 0, &img_bg);
	putimage(bird_x, bird_y, &img_bd1, NOTSRCERASE);
	putimage(bird_x, bird_y, &img_bd2, SRCINVERT);
	putimage(bar_x1, bar_y1, &img_zhuzishang1, NOTSRCERASE);
	putimage(bar_x1, bar_y1, &img_zhuzishang2, SRCINVERT);
	putimage(bar_x2, bar_y2, &img_zhuzixia1, NOTSRCERASE);
	putimage(bar_x2, bar_y2, &img_zhuzixia2, SRCINVERT);
	FlushBatchDraw();
}
void updateWithoutInput()  // 与用户输入无关的更新
{
	static int speedbird = 0, speed = 0;
	if (speedbird < 2)
		speedbird++;
	else if (speedbird == 2)
	{
		bird_y += 3;
		speedbird = 0;
	}
	if (speed < 5)
		speed += 5;
	else if (speed == 5)
	{
		bar_x1 -= 5;
		bar_x2 -= 5;
		speed = 0;
	}
	if (bar_x1 <= -140)
	{
		bar_x1 = 340;
		bar_y1 = rand() % (high - 300) - 500;
	}
	if (bar_x2 <= -140)
	{
		bar_x2 = 340;
		bar_y2 = bar_y1 + 800;
	}
	if (bird_x + 34 > bar_x1&&bird_x < bar_x1 + 140 && (!(bird_y + 24 < bar_y2 && bird_y > bar_y1 + 600)))
	{
		judge = 0;
	}

}
void updateWithInput()  // 与用户输入有关的更新
{
	MOUSEMSG m;
	if (MouseHit())
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_LBUTTONDOWN)
		{
			bird_y -= 70;
			mciSendString("close jpmusic ", NULL, 0, NULL);
			mciSendString("open D:\\jump.mp3 alias jpmusic", NULL, 0, NULL);
			mciSendString("play jpmusic ", NULL, 0, NULL);
		}
	}
}
void end()
{
	EndBatchDraw();
	getch();
	exit(0);
}
void main()
{
	startup();  // 数据初始化	
	while (1) //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();  // 与用户输入有关的更新
		if (judge == 0)
			break;
	}

	end();
}